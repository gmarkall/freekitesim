""" Convert the logfile from pickled pandas dataframe into a .mat file. """
import pandas as pd
import scipy
import numpy as np

log_file = "log_8700W_8ms.df" # 8700W average mechanical cylce power, 8 m/s wind at 6 m height

df = pd.read_pickle(log_file)

# convert v_app, that was stored as vec3 object and not as numpy object to a list of numpy arrays
df1 = df.v_app
v_apps = []
for date, row in df1.T.iteritems():
    v_apps.append(np.array(row))

mdict = {'azimuth':       df.azimuth,
         'course':        df.course,
         'depower':       df.depower,
         'elevation':     df.elevation,
         'force':         df.force,         # tether force [N] as measured on the ground
         'heading':       df.heading,
         'height':        df.height,        # height above the swivel of the ground-station
         'kite_distance': df.kite_distance, # direct kite distance: as measured via GNSS sensor
         'l_tether':      df.l_tether,      # tether length: reel-out length from the winch
         'position':      df.position,      # vector of 3D coordinates of the particle positions
         'prediction':    df.prediction,    # vector of 3D coordinates of the predicted flight path
         'set_force':     df.set_force,
         'steering':      df.steering,      # relative steering        (-1.0 .. 1.0)
         'sync_speed':    df.sync_speed,    # synchronous motor speed (set value of the reel-out speed)
         'system_state':  df.system_state,
         'time':          df.time,          # relative simulation time in seconds
         'time_rel':      df.time_rel,
         'turn_rate':     df.turn_rate,
         'v_app':         v_apps,           # apparent air velocity at the kite (x, y, z)
         'v_app_norm':    df.v_app_norm,    # norm of the apparent air velocity ath the kite
         'v_reelout':     df.v_reelout,     # reel-out speed of the winch
         'vel_kite':      df.vel_kite,      # velocity of the kite (x, y, z)
         'yaw_angle':     df.yaw_angle
         }
scipy.io.savemat(log_file + '.mat', mdict=mdict)

