This directory contains log-files of simulated and measured flights
as pickled pandas data frames.

Unpack compressed files with the command:

unxz *.xz

The script list.py lists the fields, that are contained in a log file.

The script convert.py converts a log file into a .mat file.

Uwe Fechner, 29.10.2013
