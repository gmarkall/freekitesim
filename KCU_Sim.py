# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
""" Class for simulating the kite control unit (KCU). """
import Queue
from pylab import np, plot, grid, figure, gca
from Settings import PERIOD_TIME, DEPOWER_OFFSET, HEIGHT_K, HEIGHT_B, POWER2STEER_DIST
from math import sqrt, pi, acos, degrees

DELAY_MS = 150                                      # delay in milli-seconds
DELAY = int(round(DELAY_MS / 1000.0 / PERIOD_TIME)) # delay in period times
V_DEPOWER  = 0.075            # max velocity of depowering in units per second (full range: 1 unit)
V_STEERING = 0.2              # max velocity of steering in units per second   (full range: 2 units)
DEPOWER_GAIN  = 3.0           # 3.0 means: more than 33% error -> full speed
STEERING_GAIN = 3.0
DEPOWER_DRUM_DIAMETER = 69.0e-3 * 0.97 # outer diameter of the depower drum at depower = DEPOWER_OFFSET [m]
STEERING_LINE_SAG = 0           # sag of the steering lines in percent
TAPE_THICKNESS = 6e-4           # thickness of the depower tape [m]

DepowerOffset = DEPOWER_OFFSET

def setDepowerOffset(depower_offset):
    global DepowerOffset
    DepowerOffset = depower_offset

def calcDeltaL(rel_depower):
    """ Calculate the length increase of the depower line [m] as function of the relative depower
    setting [0..1]. """
    u = DEPOWER_DRUM_DIAMETER * (100.0 + STEERING_LINE_SAG) / 100.0
    l_ro = 0.0
    rotations = (rel_depower - 0.01 * DepowerOffset) * 10.0 * 11./3. * (3918.8 - 230.8) / 4096.
    while rotations > 0.0:
        l_ro += u * pi
        rotations -= 1.0
        u -= TAPE_THICKNESS
    if rotations < 0.0:
        l_ro += (-(u + TAPE_THICKNESS) * rotations + u * (rotations + 1.0)) * pi * rotations
    return l_ro
    #return (rel_depower - 0.01 * DEPOWER_OFFSET) * 6.638

def calcAlphaDepower(rel_depower):
    """calculate the change of the angle between the kite and the last tether segment [rad] as function of the
    length increase  of the depower line delta_l [m]. """
    a   = POWER2STEER_DIST
    b_0 = HEIGHT_B + 0.5 * HEIGHT_K
    b = b_0 + 0.5 * calcDeltaL(rel_depower) # factor 0.5 due to the pulleys

    c = sqrt(a*a + b_0*b_0)
    # print 'a, b, c:', a, b, c, rel_depower
    if c >= a+b:
        return None
    else:
        return pi/2.0 - acos(1/(2*a*b)*(a*a+b*b-c*c))


class KCU_Sim(object):
    """ Implements a PI control with a maximul micro-winch speed and
    a combined motor controller and transmission delay.
    The call sequence should be:
    1. setDepowerSteering
    2. getDepowerSteering
    3. onTimer
    """
    def __init__(self):
        self._setDepower = DepowerOffset * 0.01
        self._setSteering = 0.0
        self._depower =  DepowerOffset * 0.01  #    0 .. 1.0
        self._steering = 0.0 # -1.0 .. 1.0
        self._inputQueue_depower  = Queue.Queue()
        self._inputQueue_steering = Queue.Queue()

    def clear(self):
        self._setDepower =  DepowerOffset * 0.01
        self._setSteering = 0.0
        self._depower =  DepowerOffset * 0.01  #    0 .. 1.0
        self._steering = 0.0 # -1.0 .. 1.0
        self._inputQueue_depower = Queue.Queue()
        self._inputQueue_steering = Queue.Queue()

    def setDepowerSteering(self, depower, steering):
        self._inputQueue_depower.put(depower)
        self._inputQueue_steering.put(steering)

    def getDepower(self):
        return self._depower

    def getSteering(self):
        return self._steering

    def onTimer(self, period_time = PERIOD_TIME):
        """ Should be called every 50 ms. """
        # calculate the depower motor velocity
        vel_depower = (self._setDepower - self._depower) * DEPOWER_GAIN
        if vel_depower > V_DEPOWER:
            vel_depower = V_DEPOWER
        if vel_depower < -V_DEPOWER:
            vel_depower = -V_DEPOWER
        # update the position
        self._depower += vel_depower * period_time
        # calculate the steering motor velocity
        vel_steering = (self._setSteering - self._steering) * STEERING_GAIN
        if vel_steering > V_STEERING:
            vel_steering = V_STEERING
        if vel_steering < -V_STEERING:
            vel_steering = -V_STEERING
        # update the steering position
        self._steering += vel_steering * period_time
        # feed the delayed set values int the _setPower and _setSteering attributes
        if self._inputQueue_depower.qsize() >= DELAY:
            self._setDepower = self._inputQueue_depower.get()
            self._setSteering = self._inputQueue_steering.get()

if __name__ == "__main__":
    print DepowerOffset
    X = np.linspace(0.24816, 0.42468, 256, endpoint=True) # create an array from -pi to pi
    DELTA_L, ALPHA_D = [], []
    for rel_depower in X:
        DELTA_L.append(calcDeltaL(rel_depower))
        ALPHA_D.append(degrees(calcAlphaDepower(rel_depower)))
    plot(100*X, DELTA_L, label='delta_l')
    gca().set_ylabel(u'Delta depower length [m]')
    gca().set_xlabel(u'Relative depower [%]')
    grid(True, color='0.25')
    figure()
    plot(100*X, ALPHA_D, label='alpha_depower')
    gca().set_ylabel(u'Alpha depower [°]')
    gca().set_xlabel(u'Relative depower [%]')
    grid(True, color='0.25')


