# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
This module provides the functions approximate_cl and approximate_cd for the fast
calculation of the lift and drag coefficients as function of the angle of attack.
"""
# pylint: disable=C0326

from Timer import Timer
from scipy.interpolate import InterpolatedUnivariateSpline
from numba import autojit
import numpy as np

ALPHA_CL = [-180.0, -160.0, -90.0, -20.0, -10.0,  -5.0,  0.0, 20.0, 40.0, 90.0, 160.0, 180.0]
CL_LIST  = [   0.0,    0.5,   0.0,  0.08, 0.125,  0.15,  0.2,  1.0,  1.0,  0.0,  -0.5,   0.0]

ALPHA_CD = [-180.0, -170.0, -140.0, -90.0, -20.0, 0.0, 20.0, 90.0, 140.0, 170.0, 180.0]
CD_LIST  = [   0.5,    0.5,    0.5,   1.0,   0.2, 0.1,  0.2,  1.0,   0.5,   0.5,   0.5]

calc_cl = InterpolatedUnivariateSpline(ALPHA_CL, CL_LIST)
calc_cd = InterpolatedUnivariateSpline(ALPHA_CD, CD_LIST)

INTERP_ALPHA = np.linspace(-180, 180, 1000)
INTERP_CLS = calc_cl(INTERP_ALPHA)
INTERP_CDS = calc_cd(INTERP_ALPHA)
SIZE = len(INTERP_ALPHA)

@autojit(nopython=True)
def approximate_cl(angle):
    """
    Approximate the the lift coefficient CL through linear interpolation in pre-calculated array.
    alpha: angle of attack in degrees.
    """
    size = len(INTERP_ALPHA)
    slope = 0.0
    step = INTERP_ALPHA[1]-INTERP_ALPHA[0]
    idx = int((angle-INTERP_ALPHA[0]) / step) - 1
    if idx < 0:
        idx = 0
    while idx < size - 1:
        if angle >= INTERP_ALPHA[idx] and angle <= INTERP_ALPHA[idx+1]:
            slope = (INTERP_CLS[idx+1] - INTERP_CLS[idx]) / step
            break
        idx += 1

    return INTERP_CLS[idx] + slope * (angle - INTERP_ALPHA[idx])

@autojit(nopython=True)
def approximate_cd(angle):
    """
    Approximate the drag coefficient CD through linear interpolation in pre-calculated array.
    alpha: angle of attack in degrees.
    """
    size = len(INTERP_ALPHA)
    slope = 0.0
    step = INTERP_ALPHA[1]-INTERP_ALPHA[0]
    idx = int((angle - INTERP_ALPHA[0]) / step) - 1
    if idx < 0:
        idx = 0
    while idx < size - 1:
        if angle >= INTERP_ALPHA[idx] and angle <= INTERP_ALPHA[idx+1]:
            slope = (INTERP_CDS[idx+1] - INTERP_CDS[idx]) / step
            break
        idx += 1

    return INTERP_CDS[idx] + slope * (angle - INTERP_ALPHA[idx])

# call the functions once for pre-compilation
approximate_cl(10.0)
approximate_cd(10.0)

if __name__ == "__main__":
    """ Small benchmarks. """
    with Timer() as t:
        for alpha in np.linspace(-20, 60, 200000):
            x = calc_cl(alpha)
    print "Execution time calc_cl [µs]: ", t.secs/200000 * 1e06

    with Timer() as t:
        for alpha in np.linspace(-20, 60, 200000):
            x = approximate_cl(alpha)
    print "Execution time approximate_cl [µs]: ", t.secs/200000 * 1e06
