# -*- coding: utf-8 -*-
""" Module for logging and displaying simulation results.
Can be executed from the command line to display diagrams. In this case it must be
called with the following parameters:
python Logger.py <logfile> 1 2 3
1, 2 and 3 would be the numbers of the diagrams to display. For the meaning of these
numbers look at line 466 ff of this file.
"""
# TODO: Split the class Logger into Logger and Plotter
# TODO: Add recording of the central control settings
import pandas as pd

import datetime as dt
import time as ti
import sys
from os.path import expanduser
HOME = expanduser("~")
sys.path.append(HOME + '/00PythonSoftware/FreeKiteSim')
from MetaDataFrame import MetaDataFrame, mload
import numpy as np
import scipy


class Logger(object):
    """ Class for logging simulation results as Pandas data frame.
    The results can be stored and loaded as pickled .df file.
    They can be processed with the class ProcessData and visualized
    with the class ViewData from the ProcessData module in the same
    way as the measured data.
    """
    def __init__(self, filename=None):
        self.start_time = ti.time()
        self.times, self.forces, self.set_forces = [], [], []
        self.heights, self.kite_distances, self.lengths = [], [], []
        self.v_reelouts = []
        self.sync_speeds = []
        self.v_app_norms = []
        self.v_apps = [] # apparent wind speed vectors
        self.azimuths = []
        self.elevations = []
        self.system_states = []
        self.winch_control_states = []
        self.energy = 0.0
        self.cycles = []
        self.last_rel_time = 0.0
        self.last_cycle_start = 0.0
        self.cycle = -1
        self.cycle_ = []
        self.depowers = []
        self.steerings = []
        self.turn_rates = []
        self.yaw_angles = []
        self.headings, self.courses = [], []
        self.positions = []
        self.predictions = []
        self.vel_kites = []
        self.velocities = []
        self.vels_tangential = []
        self.all_forces = []
        self.aero_forces = []
        self.alphas = [] # angle of attack
        self.cpu_times = [] # percentage of the cpu time used
        self.ds = None
        self.view = None
        self.y1 = []
        self.yd1 = []
        self.euler_parameters = []
        self.LoDs = []
        self.wind_vels = []
        self.v_tbs = [] # tether base line speed
        self.set_depowers = []
        self.set_steerings = []
        self.v_winds_kite = []
        if filename is None:
            self.df = None
        else:
            self.df = mload(filename)
            self.df.rename(columns={'l_tether': 'tetherLength'}, inplace=True)

    def clear(self, filename=None):
        """ Clear the logger instance to start a new logging session. """
        self.__init__(filename)

    def append(self, rel_time, force, height, l_tether, kite_distance, v_reelout, sync_speed):
        """ Append the essential data to the logging object. """
        self.times.append(rel_time)
        self.forces.append(force)
        self.heights.append(height)
        self.lengths.append(l_tether)
        self.kite_distances.append(kite_distance)
        self.v_reelouts.append(v_reelout)
        self.sync_speeds.append(sync_speed)
        self.energy += force * v_reelout * (rel_time - self.last_rel_time)
        self.last_rel_time = rel_time
        self.cycle_.append(self.cycle)

    def append2(self, azimuth, elevation, v_app_norm, depower, steering, yaw_angle, \
                      heading, course, wind_vel=0.0):
        """ Append additional data to the logging object. """
        self.azimuths.append(azimuth)
        self.elevations.append(elevation)
        self.v_app_norms.append(v_app_norm)
        self.depowers.append(depower)
        self.steerings.append(steering)
        self.yaw_angles.append(yaw_angle)
        self.headings.append(heading)
        self.courses.append(course)
        self.wind_vels.append(wind_vel)

    def append3(self, pos, vel_kite, v_app, prediction, alpha, velocities, cpu_time=0.0, LoD=0.0, \
                      vel_tangential=0.0, v_tb=0.0):
        """ Append the 2d array of the particle positions pos, the kite velocity vector vel_kite,
        the apparent air velocity vector v_app and the angle of attack alpha. """
        self.positions.append(pos)
        self.vel_kites.append(vel_kite)
        self.v_apps.append(v_app)
        self.predictions.append(prediction)
        self.alphas.append(alpha)
        self.velocities.append(velocities)
        self.cpu_times.append(cpu_time)
        self.LoDs.append(LoD)
        self.vels_tangential.append(vel_tangential)
        self.v_tbs.append(v_tb)

    def append4(self, set_depower, set_steering, v_wind_kite):
        """ Append the set values of depower and steering: """
        self.set_depowers.append(set_depower)
        self.set_steerings.append(set_steering)
        self.v_winds_kite.append(v_wind_kite)

    def appendSystemStateSetForce(self, system_state, set_force, winch_control_state=7):
        """ Append the system_state as defined in WinchController2.py and the set_force of the
        winch controller. """
        self.system_states.append(system_state)
        self.set_forces.append(set_force)
        self.winch_control_states.append(int(winch_control_state))

    def appendAllForces(self, forces, aero_forces):
        """ Append all forces, i.e. the forces in all points. """
        self.all_forces.append(forces)
        self.aero_forces.append(aero_forces)

    def appendY(self, y1, yd1):
        """ Append the complete state vector y. """
        self.y1.append(y1)
        self.yd1.append(yd1)

    def appendEuler(self, euler_parameters):
        """ Append the Euler parameters."""
        self.euler_parameters.append(euler_parameters)

    def _convert(self):
        """
        Convert the data, that is stored in the lists of the logger object
        into a Pandas data frame. The data frame is stored in the self.df attribute.
        """
        # fields, that are present if self.append was called
        di = {'time'      : self.times,
              'time_rel'  : self.times,
              'force'     : self.forces,
              'height'    : self.heights,
              'l_tether'  : self.lengths,
              'kite_distance' : self.kite_distances,
              'v_reelout' : self.v_reelouts,
              'sync_speed': self.sync_speeds}
        # fields, that are present if self.append2 was called additionally
        di2 = {'azimuth'  : self.azimuths,
               'elevation' : self.elevations,
               'v_app_norm': self.v_app_norms,
               'depower'   : self.depowers,
               'steering'  : self.steerings,
               'yaw_angle' : self.yaw_angles,
               'heading'   : self.headings,
               'course'    : self.courses}
        if len(self.azimuths) > 0:
            di.update(di2)
        # fields, that are present if append3 was called additionally
        di3 = {'position'   : self.positions,
               'vel_kite'   : self.vel_kites,
               'v_app'      : self.v_apps,
               'prediction' : self.predictions,
               'alpha'      : self.alphas,
               'velocities' : self.velocities,
               'cpu_time'   : self.cpu_times}
        if len(self.positions) > 0:
            di.update(di3)
        di3b = {'LoD': self.LoDs}
        if len(self.LoDs) > 0:
            di.update(di3b)
        # fields, that are present if appendSystemStateSetForce was called additionally
        di4 = {'system_state'        : self.system_states,
               'winch_control_state' : self.winch_control_states,
               'set_force'           : self.set_forces}
        if len(self.system_states) > 0:
            di.update(di4)
        # fields, that are present if appendAllForces was called additionally
        di5 = {'forces'          : self.all_forces,
               'aero_forces'     : self.aero_forces,
               'y1'              : self.y1,
               'yd1'             : self.yd1,
               'euler_parameters': self.euler_parameters}
        if len(self.all_forces) > 0:
            di.update(di5)

        di6 = {'cycle'          : self.cycle_}
        if len(self.cycle_) > 0:
            di.update(di6)

        di7 = {'vel_tangential'          : self.vels_tangential}
        if len(self.vels_tangential) > 0:
            di.update(di7)

        di8 = {'wind_vel'          : self.wind_vels}
        if len(self.wind_vels) > 0:
            di.update(di8)

        di9 = {'v_tb'          : self.v_tbs}
        if len(self.v_tbs) > 0:
            di.update(di9)

        di10 = {'set_depower'        : self.set_depowers,
                'set_steering'        : self.set_steerings,
                'v_wind_kite'         : self.v_winds_kite}

        if len(self.set_depowers) > 0:
            di.update(di10)

        self.df = pd.DataFrame(di)
        self.df.index = (dt.datetime.utcfromtimestamp(self.start_time)+(self.df['time'])*1000*pd.offsets.Milli())
        self.df.index = self.df.index.tz_localize('UTC')
        self.df.index = self.df.index.tz_convert('Europe/Amsterdam')
        self.df = MetaDataFrame(self.df)
        if len(self.cycles) > 0:
            self.df.cycles = self.cycles

    def getDataFrame(self, convert=False):
        """ Return the stored data as Pandas data frame. Convert it first, if neccessary. """
        if (self.df is None) or convert:
            self._convert()
        return self.df

    def save(self, mat=False, time_stamp='', convert=True):
        """ Save the log file as pandas data frame or as Matlab .mat file, if the parameter
        mat is True.
        """
        df = self.getDataFrame(convert)
        time_stamp = ti.strftime("%Y%m%d_%H%M%S")

        while True:
            ans = raw_input("Would you like to save the log file of the flight? y/[n]\n")
            if ans == "" or ans[0] in 'Nn':
                return
            elif ans[0] in 'Yy':
                fn = raw_input("Filename: ")
                if not fn == '':
                    filename = HOME+'/00PythonSoftware/FreeKiteSim/00_results/'+fn+".df"
                    break
                else:
                    filename = HOME + '/00PythonSoftware/FreeKiteSim/00_results/log_' + time_stamp+".df"
                    break
            else:
                print "Please answer either (y)es or (n)o"

        if mat:
            # convert v_app, that was stored as vec3 object and not as numpy object to a list of numpy arrays
            df1 = df.v_app
            v_apps = []
            for date, row in df1.T.iteritems():
                v_apps.append(np.array(row))

            mdict = {'azimuth':          df.azimuth,
                     'course':           df.course,
                     'depower':          df.depower,
                     'elevation':        df.elevation,
                     'force':            df.force,         # tether force [N] as measured on the ground
                     'forces':           df.forces,
                     'aero_forces':      df.aero_forces,
                     'heading':          df.heading,
                     'height':           df.height,        # height above the swivel of the ground-station
                     'kite_distance':    df.kite_distance, # direct kite distance: as measured via GNSS sensor
                     'l_tether':         df.l_tether,      # tether length: reel-out length from the winch
                     'position':         df.position,      # vector of 3D coordinates of the particle positions
                     #'prediction':       df.prediction,    # vector of 3D coordinates of the predicted flight path
                     'set_force':        df.set_force,
                     'steering':         df.steering,      # relative steering        (-1.0 .. 1.0)
                     #'sync_speed':       df.sync_speed,    # synchronous motor speed (set value of the reel-out speed)
                     'system_state':     df.system_state,
                     'time':             df.time,          # relative simulation time in seconds
                     'time_rel':         df.time_rel,
                     'turn_rate':        df.turn_rate,
                     'v_app':            v_apps,           # apparent air velocity at the kite (x, y, z)
                     #'v_app_norm':       df.v_app_norm,    # norm of the apparent air velocity at the kite
                     'v_reelout':        df.v_reelout,     # reel-out speed of the winch
                     'vel_kite':         df.vel_kite,      # velocity of the kite (x, y, z)
                     'velocities':       df.velocities,
                     'y1':               df.y1,
                     'yd1':              df.yd1,
                     'yaw_angle':        df.yaw_angle,
                     'euler_parameters': df.euler_parameters
                    }
            scipy.io.savemat(filename + '.mat', mdict=mdict)
        else:
            df.save(filename)
        
        print "Saved successfully"

if __name__ == "__main__":
    
    # Specify an existing file to load
    log = Logger("./00_results/log.df")
    df = log.getDataFrame()
