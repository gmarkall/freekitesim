# -*- coding: utf-8 -*-
"""
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Model of a kite-power system in implicit form: residual = f(y, yd)

This model implements a 3D mass-spring system with reel-out. It uses six tether segments (the number can be
configured in the file Settings.py). The kite is modelled using four point masses with three attached plates.
The spring constant and the damping decrease with the segment length. The aerodynamic kite forces are
calculated, depending on reel-out speed, depower and steering settings. """

import numpy as np
# pylint: disable=E0611
from numba import void, int_, double, jit, autojit
import linalg_3d as la
import math
from math import exp, radians, pi, acos
from Settings import C_SPRING, DAMPING, SEGMENTS, L_0, V_REEL_OUT, MASS, KCU_MASS, ELEVATION, \
                     V_WIND, REL_SIDE_AREA, WINCH_MODEL, AREA, \
                     PARTICLES, MASSES, SPRINGS, KITE_PARTICLES, ALPHA_ZERO, ALPHA_ZTIP, WINCH_D_TETHER

from WinchModel import calcAcceleration
from KCU_Sim import calcAlphaDepower
from Timer import Timer
from Approximate_CLCD import approximate_cl, approximate_cd

# pylint: disable=E1101
G_EARTH      = np.array([0.0, 0.0, -9.81]) # gravitational acceleration
NO_PARTICLES = SEGMENTS + KITE_PARTICLES + 1

KS = radians(16.565 * 1.064 * 0.875 * 1.033 * 0.9757 * 1.083)  # max steering

K_ds = 1.5 # influence of the depower angle on the steering sensitivity
MAX_ALPHA_DEPOWER = 31.0
K = 1 - REL_SIDE_AREA # correction factor for the drag
EPSILON = 1e-6
RHO_0 = 1.225 # kg / m³

C_0 = -0.0032 # steering offset
C_D_TETHER = 0.958 # Coefficient found by regression of test data

ALPHA = 1.0/7.0

@autojit(nopython=True)
def calcWindFactor(height):
    """ Calculate the wind speed at a given height and reference height. """
    return (height / 6.0)**ALPHA

@autojit(nopython=True)
def calcWindHeight(v_wind_gnd, height):
    return v_wind_gnd * calcWindFactor(height)

@autojit(nopython=True)
def calcRho(height):
    return RHO_0 * exp(-height / 8550.0)

def calcParticleForces_(pos1, pos2, vel1, vel2, v_wind_tether, spring, forces, stiffnes_factor, segments, \
                       d_tether, i):
    """ Calculate the drag force of the tether segment, defined by the parameters po1, po2, vel1 and vel2
    and distribute it equally on the two particles, that are attached to the segment.
    The result is stored in the array self.forces. """
    p_1 = int_(spring[0])     # Index of point nr. 1
    p_2 = int_(spring[1])     # Index of point nr. 2
    l_0 = spring[2]     # Unstressed length
    k = spring[3] * stiffnes_factor       # Spring constant
    c = spring[4]       # Damping coefficient
    #print 'k, c: ', k, c
    segment = pos1 - pos2
    rel_vel = vel1 - vel2
    av_vel = 0.5 * (vel1 + vel2)
    norm1 = la.norm(segment)
    unit_vector = segment / norm1
    k1 = 0.25 * k # compression stiffness kite segments
    k2 = 0.1 * k # compression stiffness tether segments
    # look at: http://en.wikipedia.org/wiki/Vector_projection
    # calculate the relative velocity in the direction of the spring (=segment)
    spring_vel   = la.dot(unit_vector, rel_vel)
    if (norm1 - l_0) > 0.0:
        spring_force = (k *  (norm1 - l_0) + (c * spring_vel)) * unit_vector
    elif i >= segments: # kite spring
        spring_force = (k1 *  (norm1 - l_0) + (c * spring_vel)) * unit_vector
    else:
        spring_force = (k2 *  (norm1 - l_0) + (c * spring_vel)) * unit_vector
    #print "spring_force: ", spring_force
    # Aerodynamic damping for particles of the tether and kite
    v_apparent = v_wind_tether - av_vel
    # area = norm1 * d_tether * (1 - 0.9 * abs(la.dot(unit_vector, v_apparent) / v_app_norm))
    # half_drag_force = -0.25 * 1.25 * v_app_norm * area * v_apparent
    area = norm1 * d_tether
    v_app_perp = v_apparent - la.dot(v_apparent, unit_vector) *unit_vector
    half_drag_force = -0.25 * 1.25 * C_D_TETHER * la.norm(v_app_perp) * area * v_app_perp

    forces[p_1] += half_drag_force + spring_force
    forces[p_2] += half_drag_force - spring_force

calcParticleForces2 = jit(void(double[:], double[:], double[:], double[:], double[:], double[:], \
                          double[:,:], double, int_, double, int_)) (calcParticleForces_)

#        innerLoop2(self.pos, self.vel, self.v_wind, self.v_wind_tether, SPRINGS, self.forces, \
#                    self.stiffnes_factor, int_(SEGMENTS), double(self.d_tether))

def innerLoop2_(pos, vel, v_wind, v_wind_tether, forces, stiffnes_factor, segments, d_tether):
    for i in xrange(SPRINGS.shape[0]):
        p_1 = int_(SPRINGS[i, 0])  # First point nr.
        p_2 = int_(SPRINGS[i, 1])  # Second point nr.
        height = 0.5 * (pos[p_1][2] + pos[p_2][2])
        v_wind_tether[0] = calcWindHeight(v_wind[0], height)
        v_wind_tether[1] = calcWindHeight(v_wind[1], height)
        v_wind_tether[2] = calcWindHeight(v_wind[2], height)
        calcParticleForces2(pos[p_1], pos[p_2], vel[p_1], vel[p_2], v_wind_tether, SPRINGS[i], forces, \
                            stiffnes_factor, segments, d_tether, i)

innerLoop2 = jit(void(double[:, :], double[:, :], double[:], double[:], double[:, :], double, int_, double))(innerLoop2_)

# pylint: disable=E1101
d_array = double[:]
dd_array = double[:, :]

@jit
class FastKPS(object):
    """ Class with the inner methods of the residual calculations. Must be compiled with NUMBA. """
    @void()
    def __init__(self):
        self.v_wind = d_array(np.array((V_WIND, 0.0, 0.0))) # (west wind, downwind direction to the east)
        self.v_wind_gnd = d_array(np.array((V_WIND, 0.0, 0.0))) # (west wind, downwind direction to the east)
        self.v_wind_tether = d_array(np.array((V_WIND, 0.0, 0.0))) # wind at half of the height of the kite
        self.length = double(0.0)   # length of one tether segment at zero force
        self.c_spring = double(0.0) # spring constant of one tether segment
        self.damping = double(0.0)  # damping constant of one tether segment
        self.alpha_depower = double(0.0)
        self.steering = double(0.0)
        self.lift_ = double(0.0)
        self.drag_ = double(0.0)
        self.last_alphas = d_array(np.array((0.1, ALPHA_ZTIP, ALPHA_ZTIP)))
        self.v_app_2 = d_array(np.zeros(3))   # last apparent air velocity
        self.last_force2 = d_array(np.zeros(3)) # sum of the aerodynamic forces, acting on the kite
        self.alpha_zero = double(0.0)
        self.stiffnes_factor = double(0.04)
        self.pos = dd_array(np.zeros((SEGMENTS + KITE_PARTICLES + 1, 3), order='F'))
        self.vel = dd_array(np.zeros((SEGMENTS + KITE_PARTICLES + 1, 3), order='F'))
        # array of force vectors, one for each particle
        self.forces = dd_array(np.zeros((SEGMENTS + PARTICLES.shape[0] - 1, 3), order='F'))
        self.xyz = dd_array(np.zeros((3, 3), order='F'))

        self.zeros = d_array(np.zeros(3))
        self.d_tether = double(WINCH_D_TETHER)

    @void(double[:,:], double[:,:], double, double, double)
    def calcAeroForces4_(self, pos, vel, rho, alpha_depower, rel_steering):

        #pos0, pos3, pos4: position of the kite particles P0, P3, and P4
        #v2, v3, v4:       velocity of the kite particles P2, P3, and P4
        #rho:              air density [kg/m^3]
        #rel_depower:      value between  0.0 and  1.0
        #rel_steering:     value between -1.0 and +1.0
        L2, L3, L4 = d_array(np.zeros(3)), d_array(np.zeros(3)), d_array(np.zeros(3))
        D2, D3, D4 = d_array(np.zeros(3)), d_array(np.zeros(3)), d_array(np.zeros(3))
        self.pos[:] = pos
        self.vel[:] = vel
        pos2, pos3, pos4 = pos[SEGMENTS+2], pos[SEGMENTS+3], pos[SEGMENTS+4]
        v2, v3, v4 = vel[SEGMENTS+2], vel[SEGMENTS+3], vel[SEGMENTS+4]
        va_2, va_3, va_4 = self.v_wind - v2, self.v_wind - v3, self.v_wind - v4
        pos_centre = 0.5 * (pos3 + pos4)
        delta = pos2 - pos_centre
        z = -la.normalize(delta)
        y = la.normalize(pos3 - pos4)
        x = la.cross(y, z)

        va_xz2 = va_2 - la.dot(va_2, y) * y
        if la.norm(va_xz2) < EPSILON:
            alpha_2 = np.nan
        else:
            dot2 = double(la.dot(la.normalize(va_xz2), x))
            alpha_2 = (pi - acos(la.limit(dot2)) - alpha_depower) * 360.0 / pi + self.alpha_zero
            CL2, CD2 = approximate_cl(alpha_2), approximate_cd(alpha_2)
            DIR2 = la.cross(va_2, y)
            if la.norm(DIR2) > EPSILON:
                L2 = -0.5 * rho * (la.norm(va_xz2))**2 * AREA * CL2 * la.normalize(DIR2)
            else:
                L2 = self.zeros
            D2 = -0.5 * K * rho * la.norm(va_2) * AREA * CD2 * va_2
            self.forces[SEGMENTS + 2] += (L2 + D2)

        va_xy3 = va_3 - la.dot(va_3, z) * z
        if la.norm(va_xy3) < EPSILON:
            alpha_3 = np.nan
        else:
            dot3 = double(la.dot(la.normalize(va_xy3), x))
            alpha_3 = (pi - acos(la.limit(dot3)) - rel_steering * KS) * 360.0 / pi + ALPHA_ZTIP
            CL3, CD3 = approximate_cl(alpha_3), approximate_cd(alpha_3)
            DIR3 = la.cross(va_3, z)
            if la.norm(DIR3) > EPSILON:
                L3 = -0.5 * rho * (la.norm(va_xy3))**2 * AREA * REL_SIDE_AREA * CL3 * la.normalize(DIR3)
            else:
                L3 = self.zeros

            D3 = -0.5 * K * rho * la.norm(va_3) * AREA * REL_SIDE_AREA * CD3 * va_3
            if math.isnan(alpha_3):
                print "Error: alpha_3", va_xy3, x, rel_steering, KS, ALPHA_ZTIP
                print "--> ", la.dot(la.normalize(va_xy3), x)
            if math.isnan(L3.sum()):
                print "Error: L3"
            if math.isnan(D3.sum()):
                print "Error: D3"
            self.forces[SEGMENTS + 3] += (L3 + D3)

        va_xy4 = va_4 - la.dot(va_4, z) * z
        if la.norm(va_xy4) < EPSILON:
            alpha_4 = np.nan
        else:
            dot4 = double(la.dot(la.normalize(va_xy4), x))
            alpha_4 = (pi - acos(la.limit(dot4)) + rel_steering * KS) * 360.0 / pi + ALPHA_ZTIP
            CL4, CD4 = approximate_cl(alpha_4), approximate_cd(alpha_4)
            DIR4 = la.cross(z, va_4)
            if la.norm(DIR4) > EPSILON:
                L4 = -0.5 * rho * (la.norm(va_xy4))**2 * AREA * REL_SIDE_AREA * CL4 * la.normalize(DIR4)
            else:
                L4 = self.zeros
            D4 = -0.5 * K * rho * la.norm(va_4) * AREA * REL_SIDE_AREA * CD4 * va_4
            if math.isnan(alpha_4):
                print "Error: alpha_4", va_xy4, x, rel_steering, KS, ALPHA_ZTIP
                print "--> ", la.dot(la.normalize(va_xy4), x)
            if math.isnan(L4.sum()):
                print "Error: L4"
            if math.isnan(D4.sum()):
                print "Error: D4"
            self.forces[SEGMENTS + 4] += (L4 + D4)

        self.xyz[0] = x
        self.xyz[1] = y
        self.xyz[2] = z
        self.lift_ = la.norm(L2)
        self.drag_ = la.norm(D2+D3+D4)
        self.last_alphas[0] = alpha_2
        self.last_alphas[1] = alpha_3
        self.last_alphas[2] = alpha_4

    @void(double[:, :], double[:, :], double[:, :], double[:, :], double[:, :], double[:, :])
    def loop(self, pos, vel, posd, veld, res0, res1):
        """ Calculate the vector res0 using a vector expression, and calculate res1 using a loop
            that iterates over all tether segments. """
        res0[0] = pos[0]
        res1[0] = vel[0]
        res0[1:NO_PARTICLES] = vel[1:NO_PARTICLES] - posd[1:NO_PARTICLES]
        # Compute the masses and forces
        l_0 = self.length
        m_tether_particle = MASS * l_0 / L_0
        MASSES[SEGMENTS] = KCU_MASS + 0.5 * m_tether_particle
        for i in xrange(0, SEGMENTS):
            MASSES[i] = m_tether_particle
            SPRINGS[i, 2] = self.length # Current unstressed length
            SPRINGS[i, 3] = self.c_spring / self.stiffnes_factor
            SPRINGS[i, 4] = self.damping
        innerLoop2(self.pos, self.vel, self.v_wind, self.v_wind_tether, self.forces, \
                    self.stiffnes_factor, int_(SEGMENTS), double(self.d_tether))
        for i in xrange(1, NO_PARTICLES):
            res1[i] = veld[i] - (G_EARTH - self.forces[i] / MASSES[i])

class KPS(FastKPS):
    """ Class, that defines the physics of a kite-power system. """
    def __init__(self):
        FastKPS.__init__(self)
        self.res = np.zeros((SEGMENTS + PARTICLES.shape[0] - 1) * 6).reshape((2, -1, 3))
        if WINCH_MODEL:
            self.res = np.append(self.res, 0.0) # res_length
            self.res = np.append(self.res, 0.0) # res_v_reel_out
        self.t_0 = 0.0 # relative start time of the current time interval
        self.v_reel_out = 0.0
        self.sync_speed = 0.0
        self.l_tether = L_0 * SEGMENTS
        self.res0, self.res1 = self.res[0:-2].reshape((2, -1, 3))[0], self.res[0:-2].reshape((2, -1, 3))[1]
        self.rho = RHO_0

    def clear(self):
        """ Clear all member variables. """
        self.__init__()

    def residual(self, time, state_y, der_yd):
        """
        N-point tether model:
        Inputs:
        State vector state_y   = pos0, pos1, ..., posn-1, vel0, vel1, ..., veln-1, length, v_reel_out
        Derivative   der_yd    = vel0, vel1, ..., veln-1, acc0, acc1, ..., accn-1, lengthd, v_reel_out_d
        Output:
        Residual     res = res0, res1 = pos0,  ..., vel0, ...
        """
        # Reset the force vector to zero.
        self.forces.fill(0.0)

        length = state_y[-2]
        v_reel_out = state_y[-1]
        lengthd = der_yd[-2]
        v_reel_outd = der_yd[-1]
        part  = state_y[0:-2].reshape((2, -1, 3))  # reshape the state vector to a vector of particles
        # reshape the state derivative to a vector of particles derivatives
        partd = der_yd[0:-2].reshape((2, -1, 3))

        pos, vel = part[0], part[1]
        posd, veld = partd[0], partd[1]

        sync_speed = self.sync_speed
        self.length = length / SEGMENTS

        self.c_spring = C_SPRING * L_0 / self.length
        self.damping  = DAMPING  * L_0 / self.length
        self.calcAeroForces4_(pos, vel, self.rho, self.alpha_depower, self.steering)
        #tmp = math.isnan(self.forces.sum())
        #if tmp:
        #    print self.forces
        #assert not tmp, 'after calcAeroForces4'
        self.loop(pos, vel, posd, veld, self.res0, self.res1)
        self.res[-2] = lengthd - v_reel_out
        self.res[-1] = v_reel_outd - calcAcceleration(sync_speed, v_reel_out, la.norm(self.forces[0]), True)
        return self.res

    def setSyncSpeed(self, sync_speed, t_0):
        """ Setter for the reel-out speed. Must be called every 50 ms (before each simulation).
        It also updates the tether length, therefore it must be called even if v_reelout has
        not changed. """
        # self.last_sync_speed = self.sync_speed
        self.sync_speed = sync_speed
        self.t_0 = t_0
        if t_0 <= 5.0:
            self.alpha_zero = t_0/5.0 * ALPHA_ZERO
        # increase the stiffness of the bridle from 4% to 100% within the first 10 seconds
        if t_0 <= 10.0:
            self.stiffnes_factor = 9.6 * t_0 / 100.0 + 0.04

    def setDepowerSteering(self, depower, steering):
        """ Setter depower and the steering model inputs. Valid range for steering: -1.0 .. 1.0.
        Valid range for depower: 0 .. 1.0 """
        self.alpha_depower = calcAlphaDepower(depower) * (MAX_ALPHA_DEPOWER / 31.0)
        self.steering = (steering - C_0) / (1.0 + K_ds * (self.alpha_depower / radians(MAX_ALPHA_DEPOWER)))

    def setL_Tether(self, l_tether):
        """ Setter for the tether reel-out lenght (at zero force). """
        self.l_tether = l_tether

    def getL_Tether(self):
        """ Getter for the tether reel-out lenght (at zero force). """
        return self.l_tether

    def getLoD(self):
        return self.lift_ / self.drag_

    def getAlpha2(self):
        return self.last_alphas[0]

    def getXYZ(self):
        #return self.vec_x, self.vec_y, self.vec_z
        return self.xyz[0], self.xyz[1], self.xyz[2]

    def getForce(self):
        """ Return the absolute value of the force at the winch as calculated during the last
        simulation. """
        return la.norm(self.forces[0]) # last_force is the force at the whinch!

    def getV_Wind(self):
        """ Return the vector of the wind velocity at the height of the kite. """
        return self.v_wind

    def setV_WindGround(self, height, v_wind_gnd=V_WIND, wind_dir=0.0):
        """ Set the vector of the wind-velocity at the height of the kite. As parameter the height
        and the ground wind speed is needed.
        Must be called every 50 ms.
        """
        if height < 6.0:
            height = 6.0
        self.v_wind[0] = v_wind_gnd * calcWindFactor(height) * math.cos(wind_dir)
        self.v_wind[1] = v_wind_gnd * calcWindFactor(height) * math.sin(wind_dir)
        self.v_wind[2] = 0.0
        # print 'v_wind[0]', self.v_wind[0], calcWindHeight(v_wind_gnd, height)
        self.v_wind_gnd[0] = v_wind_gnd * math.cos(wind_dir)
        self.v_wind_gnd[1] = v_wind_gnd * math.sin(wind_dir)
        # self.v_wind_tether[0] = calcWindHeight(v_wind_gnd, height / 2.0)
        self.rho = calcRho(height)

    def initTether(self):
        print "KPS4.initTether()..................", WINCH_D_TETHER, "m"
        self.d_tether = WINCH_D_TETHER

    def init(self):
        """ Calculate the initial conditions y0, yd0 and sw0. Tether with the given elevation angle,
            particle zero fixed at origin. """
        self.initTether()
        DELTA = 1e-6
        # self.setCL_CD(10.0 / 180.0 * math.pi)
        pos, vel, acc = [], [], []
        state_y = DELTA
        vel_incr = 0
        sin_el, cos_el = math.sin(ELEVATION / 180.0 * math.pi), math.cos(ELEVATION / 180.0 * math.pi)
        for i in range(SEGMENTS + 1):
            radius = - i * L_0
            if i == 0:
                pos.append(np.array([-cos_el * radius, DELTA, -sin_el * radius]))
                vel.append(np.array([DELTA, DELTA, DELTA]))
            else:
                pos.append(np.array([-cos_el * radius, state_y, -sin_el * radius]))
                if i < SEGMENTS:
                    vel.append(np.array([DELTA, DELTA, -sin_el * vel_incr*i]))
                else:
                    vel.append(np.array([DELTA, DELTA, -sin_el * vel_incr*(i-1.0)]))
            acc.append(np.array([DELTA, DELTA, -9.81]))
        state_y0, yd0 = np.array([]), np.array([])
        for i in range(SEGMENTS + 1):
            state_y0  = np.append(state_y0, pos[i]) # Initial state vector
            yd0 = np.append(yd0, vel[i]) # Initial state vector derivative
        # kite particles (pos and vel)
        for i in range(KITE_PARTICLES):
            state_y0  = np.append(state_y0, (PARTICLES[i+2])) # Initial state vector
            yd0 = np.append(yd0, (np.array([DELTA, DELTA, DELTA]))) # Initial state vector derivative
        for i in range(SEGMENTS + 1):
            state_y0  = np.append(state_y0, vel[i]) # Initial state vector
            yd0 = np.append(yd0, acc[i]) # Initial state vector derivative
        # kite particles (vel and acc)
        for i in range(KITE_PARTICLES):
            state_y0  = np.append(state_y0, (np.array([DELTA, DELTA, DELTA]))) # Initial state vector
            yd0 = np.append(yd0, (np.array([DELTA, DELTA, DELTA]))) # Initial state vector derivative
        self.setL_Tether(L_0 *  SEGMENTS)
        self.setSyncSpeed(V_REEL_OUT, 0.)
        # append the initial reel-out length and it's derivative
        state_y0 = np.append(state_y0, L_0 *  SEGMENTS)
        yd0 = np.append(yd0, V_REEL_OUT)
        # append reel-out velocity and it's derivative
        state_y0 = np.append(state_y0, V_REEL_OUT)
        yd0 = np.append(yd0, DELTA)
        return state_y0, yd0

if __name__ == '__main__':
    PRINT = True
    MY_KPS = KPS()
    Y0, YD0 = MY_KPS.init() # basic consistency test; the result should be zero
    if PRINT:
        print 'y0:' , Y0
        print 'yd0:', YD0
    LOOPS = 100
    RES = MY_KPS.residual(0.0, Y0, YD0)
    with Timer() as t:
        for j in range(LOOPS):
            RES = MY_KPS.residual(0.0, Y0, YD0)

    if PRINT:
        if WINCH_MODEL:
            print 'res:', (RES[0:-2].reshape((2, -1, 3)))
            print 'res_winch', RES[-2], RES[-1]
            MY_KPS.setV_WindGround(100.0)
        else:
            print 'res:', (RES.reshape((2, -1, 3)))
        print "\nExecution time (µs): ", t.secs / LOOPS * 1e6
    # Original execution time: about 70 µs
    # New code, 11 instead of 7 particles: 87-105 µs
    # New code, correct calculation of all forces: 253 µs
    # New code, cl and cd calculation in C: 100.9, 101.7, 99.5 µs (home PC)
    # dito, work PC: 94.3, 92.3, 97.4 µs
    # dito, some refactoring: 85 µs
    # dito, with inner_loop implemented in C++: 46 µs      (work PC)
    # dito, with calcAeroForces4 implemented in C++: 35 µs (home PC)
    # dito, with calcAeroForces4 implemented in C++: 34 µs (work PC)
    # dito, on tuxedo laptop: 24 µs
    # dito, Hydra_flight_06d: 30 µs
    # dito, Hydra_flight_06d on tuxedo: 22 µs

