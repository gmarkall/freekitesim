.. footer::

   \- ###Page### -


Free Kite Power System Simulator
================================

This project aims at the development of a free kite-power system simulator
for educational purposes.

Intended use:

a) Provide a learning environment for students who are studying aiborne
   wind energy.
b) Training of kite pilots and winch operators. In this case kite and winch
   are operated with joysticks.
c) Development and test of automatic control software for kite-power systems.

It is suggested to read [Fechner2012b]_ to get some background
information about the modular software structure, that is used by the
kite-power group of TU Delft.

.. [Fechner2012b] U. Fechner, R. Schmehl: `"Design of a Distributed Kite Power Control
   System" <http://www.kitepower.eu/images/stories/publications/fechner12b.pdf>`_ .
   2012 IEEE Multi-Conference on Systems and Control, Dubrovnik, Croatia, 3-5 October 2012.

Design goals
------------

- Realtime capable, at least on fast PC's;
- component based code structure;
- high level of physical accuracy;
- easy to install and extend;
- well documented and easy to modify.
Currently the software is only tested on Ubuntu 12.04. Probably it will also run on other Linux distributions
and on Macintosh. It is not Windows compatible, because Windows has a very bad real-time behaviour and
because installing libraries from source code is very hard.

Software structure
------------------

The simulator is currently using the following software structure:

|

.. figure:: https://dl.dropboxusercontent.com/u/9075004/SoftwareStructure.png
   :width: 85%
   :align: center
   :alt: Software structure

The following seven programs are running during a simulation, one after the simulation:

GUI programs (blue)
'''''''''''''''''''
- KiteViewer

  Displays a 3D view of kite and tether.

- FrontView

  Displays the position of the kite, projected on the small earth (half sphere).

- ProtoLogger (depreciated)

  Currently only used to start and stop the simulation. This functionallity shall be moved to
  the GUI program CentralControl.

- CentralControl

  Software to start, stop and configure the flight-path-planner and the flight-path controller.
  Currently implemented as C++ GUI program, but could be replaced by a Python script.

Text-Mode scripts (green)
'''''''''''''''''''''''''
- RTSimulator

  Script that is running one of the kite-power-system models. It uses an implicit equation solver
  from the Assimulo solver suite  (http://www.jmodelica.org/assimulo).

- Logger

  During the simulation used by RTSimulator to log all data that might be interesting, e.g. the kite
  position and orientation, tether force, reel-out speed, mechanical power.

  If used as stand-alone script it can visualize logged data using Matplotlib.

Daemons (green)
'''''''''''''''
The only difference between a text-mode script and a daemon is that a daemon is running in the
background, the text output is not needed unless for debugging purposes.

- FlightPathController

  Receives desired flight trajectories from the CentralControl component. Tries to keep the kite on
  the desired trajectory. Sends steering commands to the RTSimulator.

Input/ Output (IO-) components (yellow)
'''''''''''''''''''''''''''''''''''''''

- Winch joystick

  Allows to control the reel-out speed of the tether. For reeling in additionally a button needs
  to be pressed.

- Kite joystick

  Allows to steer the kite, but also to change the angle of attack of the kite (powering or depowering).

.. raw:: pdf

   PageBreak

Communication between the software components
---------------------------------------------

The RTSimulator (or alternatively the log-file reader) is publishing ParticleSystem messages using a
ZeroMQ socket. These messages are published every 50 ms. The protocol buffer encoded messages
use the following two vector definitions for velocity and position:

|

.. code-block:: protobuf

    // EG reference frame.
    message Velocity_EG {           // velocity vector of the kite in the "EG" reference frame
      optional float v_east  = 1;   // v_east  [m/s]
      optional float v_north = 2;   // v_north [m/s]
      optional float v_up    = 3;   // v_up    [m/s]
    }
    message PositionEG {
       optional float pos_east   = 1; // pos. of the kite in east direction relative to the groundstation  [m]
       optional float pos_north  = 2; // pos. of the kite in north direction relative to the groundstation [m]
       optional float height     = 3; // height of the kite relative to the groundstation                  [m]
    }


Using these type definitions, the following messages are sent. Remark: Currently the list of the
particle velocities is not visualized, but this could be added in the future.

|

.. code-block:: protobuf

    // the state of the particle system (tether, kite, later perhaps also the winch)
    // only the information needed for 3D visualization
    message ParticleSystem {
      required double time          =  1; // relative simulation time in seconds
      required double counter       =  2;
      repeated PositionEG positions =  3; // list of particle positions in EG reference frame
      optional double v_reel_out    =  4; // reel-out velocity                [m/s]
      optional double l_tether      =  5; // tether length                    [m/s]
      optional double force         =  6; // tether force                     [N]
      optional double v_wind_ground =  7; // ground wind speed                [m/s]
      optional double height        =  8; // height of the kite               [m]
      optional double v_wind_height =  9; // wind speed at height of the kite [m/s]
      optional double v_app_x       = 10; // apparent wind speed at the kite,  x direction (east)  [m/s]
      optional double v_app_y       = 11; // wind speed at height of the kite, y direction (north) [m/s]
      optional double v_app_z       = 12; // wind speed at height of the kite, z direction (up)    [m/s]
      optional double rel_steering  = 13; // relative steering        (-1.0 .. 1.0)
      optional double rel_depower   = 14; // relative depower         ( 0.0 .. 1.0)
      optional double azimuth       = 15; // azimuth
      optional double elevation     = 16; // elevation
      repeated Velocity_EG velocities = 17; // list of particle velocities in EG reference frame
      repeated PositionEG pos_predicted = 18; // list of predicted particle positions in EG reference frame
    }

.. raw:: pdf

   PageBreak

License
-------

This software is published under the LGPL license.

GUI programs that use PyQT are licensed under the GPL (this could be changed on request by replacing
PyQT with PySide.)

If you have any suggestions and fixes to improve this software please write
an email to: u.fechner@tudelft.nl.

Related open source projects
----------------------------

Currently I know of the following open source projects in the field of simulation, optimization and
control of airborne wind energy systems:

-

Roadmap
-------

========  ========= ============================================================================= ========
**Year**  **Month** **Feature**                                                                   **Progress**
========  ========= ============================================================================= ========
2013          10    Implement Log-file player (Python).                                           \- DONE -
.             11    Add example for usage of protocol buffer encoded message over zeromq sockets. .
.             11    Publish Frontview component (C++).                                            \- DONE -
.             12    Add example for usage of the Assimulo differential equation solvers.          \- DONE -
2014          01    Add tether model.                                                             .
.             02    Add point mass kite model.                                                    .
.             03    Reimplement Frontview component in Python (help needed).                      .
.             04    .                                                                             .
.             05    Reimplement CentralControl component in Python (help needed).                 .
.             06    .                                                                             .
.             07    Publish four point kite model.                                                \- DONE -
.             08    Reimplement FlightPathController in Python.                                   .
.             09    .                                                                             .
.             10    Publish WinchController in Python.                                            \- DONE -
========  ========= ============================================================================= ========

Ideas
-----

- Implement a browser based viewer, e.g. with html5 and webgl.
- Implement a Twing model.
- Build debian packages for easy installation.
- Reduce the number of needed programs from 7 to 4.

|

Uwe Fechner, TU Delft, Section Windenergy, 14.08.2013

