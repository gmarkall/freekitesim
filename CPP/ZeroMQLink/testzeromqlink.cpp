/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "testzeromqlink.h"
#include "QsLog.h"
#include "QsLogDest.h"
#include <QByteArray>

TestZeroMQLink::TestZeroMQLink(QObject *parent) :
    QObject(parent)
{
    QString inputs = "tcp://localhost:5575";
    QStringList logger_inputs = inputs.split(" ");
    QString logger_output = "tcp://*:5576";
    m_zeroMQLink  = new ZeroMQLink();
    if (m_zeroMQLink->bind(logger_output)) {
        QLOG_ERROR() << "Couldn't bind to port: " << logger_output;
    } else {
        QLOG_INFO() << "logger_inputs: " << logger_inputs;
        int result = 0;
        for(int i=0; i < logger_inputs.length(); i++) {
            QString logger_input  = logger_inputs[i];
            result = m_zeroMQLink->connect_link(logger_input);
            if (!result) {
                QLOG_INFO() << "Listening on: " << logger_input;
            } else {
                QLOG_ERROR() << "Couldn't connect to: " << logger_input << "!";
                break;
            }
        }
        if (result) {
            QLOG_ERROR() << "Couldn't connect to: " << inputs;
        } else {
            QLOG_INFO() << "TestZeroMQLink started.";
        }
        m_zeroMQLink->subscribe(asset::system::mtEventMsg);
//        m_zeroMQLink->subscribe(asset::system::mtGroundState);
//        m_zeroMQLink->subscribe(asset::system::mtPodState);
//        m_zeroMQLink->subscribe(asset::system::mtStatistics);
//        m_zeroMQLink->subscribe(asset::system::mtPodCtrl);
        m_zeroMQLink->subscribe(asset::system::mtEstimatedSystemState);
//        m_zeroMQLink->subscribe(asset::system::mtAutopilotLog);
//        m_zeroMQLink->subscribe(asset::system::mtPilotCtrlState);
//        m_zeroMQLink->subscribe(asset::system::mtDesiredTrajectory);
//        m_zeroMQLink->subscribe(asset::system::mtBearing);
//        m_zeroMQLink->subscribe(asset::system::mtMiniWinchState);
//        m_zeroMQLink->subscribe(asset::system::mtWinchState);
        connect(m_zeroMQLink, SIGNAL(messageReceived(int)),  this,
                              SLOT(processPendingData(int)), Qt::QueuedConnection);
        m_zeroMQLink->start();
    }
}

void TestZeroMQLink::test()
{
    QLOG_INFO() << "Running tests...";
}

void TestZeroMQLink::processPendingData(int msg_type)
{
    QByteArray message = m_zeroMQLink->getLastMessage(msg_type);
    QLOG_INFO() << "Last_msg_type: " << msg_type;
    QLOG_INFO() << "Message: " << message.toHex();
}
