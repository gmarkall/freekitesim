/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "displayredceiling.h"
#include "QsLog.h"

DisplayRedCeiling::DisplayRedCeiling()
{
    QLOG_DEBUG() << "Created DisplayRedCeiling...";
    m_height = 250;
}

void DisplayRedCeiling::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipRect( option->exposedRect );
    (void) widget;
    painter->translate(0,125);
    QPen redPen(Qt::red, 2, Qt::SolidLine,Qt::RoundCap);
    QBrush redBrush(Qt::red);
    painter->setPen(redPen);
    painter->setBrush(redBrush);

    QRectF ellipse( -250, -250, 500, 500);
    painter->drawEllipse(ellipse);

    QPen whitePen(Qt::white, 2, Qt::SolidLine,Qt::RoundCap);
    QBrush whiteBrush(Qt::white);
    painter->setPen(whitePen);
    painter->setBrush(whiteBrush);

    QRectF rectangle( -250, -m_height, 500, 500);
    painter->drawRect(rectangle);

    QLOG_DEBUG() << "DisplayRedCeiling: painted...";

}

QRectF DisplayRedCeiling::boundingRect() const
{
    //return QRectF(-250, -125, 500, 250);
    return QRectF(-280, -150, 560, 300);
}

void DisplayRedCeiling::setCeilingParameters(double height)
{
    prepareGeometryChange();
    m_height = height;
    QLOG_DEBUG() << "DisplayRedCeiling: parameters updated...";
}
