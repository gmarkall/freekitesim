/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "frontviewsettings.h"
#include "QsLog.h"

// constructor
FrontViewSettings::FrontViewSettings(QString fileName)
{
    Q_ASSERT(fileName!="");
    settings = new QSettings("ASSET", fileName);
    Q_ASSERT(settings);
    if (this->settings->allKeys().count() <= 1) {
        QLOG_DEBUG() << "Creating default settings...";
        createDefaultSettings();
    }
    this->read();
}

FrontViewSettings::~FrontViewSettings()
{
}

// setters
void FrontViewSettings::setInputSources(QString inputSources)
{
    this->inputSources = inputSources;
}

void FrontViewSettings::setPeriod(int period_ms)
{
    this->period_ms = period_ms;
}

void FrontViewSettings::setTrailLength(int trailLength)
{
    this->trailLength = trailLength;
}

void FrontViewSettings::setHeadingScale(double headingScale)
{
    this->headingScale = headingScale;
}

void FrontViewSettings::setCourseScale(double courseScale)
{
    this->courseScale = courseScale;
}

void FrontViewSettings::setCeilingHeight(double ceilingHeight)
{
    this->ceilingHeight = ceilingHeight;
}

void FrontViewSettings::setTrajectoryPointSize(int trajectoryPointSize)
{
    this->trajectoryPointSize = trajectoryPointSize;
}

void FrontViewSettings::setTrajectoryStepSize(int stepSize)
{
    this->trajectoryStepSize = stepSize;
}

// getters
QString FrontViewSettings::getAllKeys()
{
    return (this->settings->allKeys().join(" "));
}

QSettings * FrontViewSettings::getSettings()
{
    return this->settings;
}

QString FrontViewSettings::getInputSources()
{
    return this->inputSources;
}

int FrontViewSettings::getPeriod()
{
    return this->period_ms;
}

int FrontViewSettings::getTrailLength()
{
    return this->trailLength;
}

double FrontViewSettings::getHeadingScale()
{
    return this->headingScale;
}

double FrontViewSettings::getCourseScale()
{
    return this->courseScale;
}

double FrontViewSettings::getCeilingHeight()
{
    return this->ceilingHeight;
}

double FrontViewSettings::getTrajectoryPointSize()
{
    return this->trajectoryPointSize;
}

int FrontViewSettings::getTrajectoryStepSize()
{
    return this->trajectoryStepSize;
}

// methods
void FrontViewSettings::createDefaultSettings()
{
    this->setInputSources(QString("tcp://localhost:5560 tcp://localhost:5561 tcp://localhost:5562 tcp://localhost:5568 tcp://localhost:5569 tcp://localhost:5570 tcp://localhost:5575"));
    this->setPeriod(20);
    this->setTrailLength(200);
    this->setHeadingScale(50.0);
    this->setCourseScale(50.0);
    this->setCeilingHeight(300.0);
    this->setTrajectoryPointSize(5);
    this->setTrajectoryStepSize(10);
    write();
}

void FrontViewSettings::write() // write the settings to QSettings data
{
    settings->setValue("inputSources",this->inputSources);
    settings->setValue("period_ms",this->period_ms);
    settings->setValue("trailLength",this->trailLength);
    settings->setValue("headingScale",this->headingScale);
    settings->setValue("courseScale",this->courseScale);
    settings->setValue("ceilingHeight",this->ceilingHeight);
    settings->setValue("trajectoryPointSize",this->trajectoryPointSize);
    settings->setValue("trajectoryStepSize",this->trajectoryStepSize);
}

void FrontViewSettings::read() // read the settings from QSettings data
{
    this->inputSources  = settings->value("inputSources").toString();
    this->period_ms     = settings->value("period_ms").toInt();
    this->trailLength   = settings->value("trailLength").toInt();
    this->headingScale  = settings->value("headingScale").toDouble();
    this->courseScale   = settings->value("courseScale").toDouble();
    this->ceilingHeight = settings->value("ceilingHeight").toDouble();
    this->trajectoryPointSize = settings->value("trajectoryPointSize").toInt();
    this->trajectoryStepSize = settings->value("trajectoryStepSize").toInt();
}
