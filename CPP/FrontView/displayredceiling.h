/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef DISPLAYREDCEILING_H
#define DISPLAYREDCEILING_H

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include <QDebug>
#include <cmath>

// Responsibility: Drawing of the ceiling limit
class DisplayRedCeiling : public QGraphicsItem
{
public:
    // constructor
    DisplayRedCeiling();

    // functions
    QRectF boundingRect() const;    // Implementation of a pure virtual public functions of a QGraphicsItem
                                    // returns an estimate of the area painted by the item. necessary because
                                    // it is called by the scene where the item is placed.

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
                                    // Implementation of a pure virtual public functions of a QGraphicsItem
                                    // paints ceiling limit

    // setters
    void setCeilingParameters(double m_height);
                                    // Sets class attributes height and width of ceiling chord (ceiling
                                    // is drawn using painter.drawChord()).

private:
    // attributes
    double m_height;                  // Height of rectangle that defines the ceiling chord

};

#endif // DISPLAYREDCEILING_H
