/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "displaybackground.h"
#include "QsLog.h"
#include "math.h" // for M_PI

DisplayBackground::DisplayBackground()
{
    computeLongitude();
    computeLatitude();
    QLOG_DEBUG() << "Created DisplayBackground...";
}

QRectF DisplayBackground::boundingRect() const
{    
    return QRectF(-280, -150, 560, 300);
}

void DisplayBackground::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipRect( option->exposedRect );
    (void) widget;
    painter->translate(0, 125);
    QPen blackPen(Qt::black, 1, Qt::SolidLine, Qt::RoundCap);
    QBrush whiteBrush(Qt::white);
    painter->setPen(blackPen);
    painter->setBrush(whiteBrush);
    QFont backgroundFont("Helvetica [Cronyx]", 8);
    painter->setFont(backgroundFont);

    for(int i = 0; i < longitudePolygons.size(); i++) {
        painter->drawPolyline(longitudePolygons[i]);
    }
    for(int i = 0; i < latitudePolygons.size(); i++) {
        painter->drawPolyline(latitudePolygons[i]);
    }

    painter->drawText(  33, 0, 20, 20, Qt::AlignCenter, QString::number( 10, 'f', 0));
    painter->drawText(  75, 0, 20, 20, Qt::AlignCenter, QString::number( 20, 'f', 0));
    painter->drawText( 115, 0, 20, 20, Qt::AlignCenter, QString::number( 30, 'f', 0));
    painter->drawText( 150, 0, 20, 20, Qt::AlignCenter, QString::number( 40, 'f', 0));
    painter->drawText( 182, 0, 20, 20, Qt::AlignCenter, QString::number( 50, 'f', 0));
    painter->drawText( 207, 0, 20, 20, Qt::AlignCenter, QString::number( 60, 'f', 0));
    painter->drawText( 240, 0, 20, 20, Qt::AlignCenter, QString::number( 90, 'f', 0));

    painter->drawText( -10, 0, 20, 20, Qt::AlignCenter, QString::number(  0, 'f', 0));
    painter->drawText( -53, 0, 20, 20, Qt::AlignCenter, QString::number( 10, 'f', 0));
    painter->drawText( -95, 0, 20, 20, Qt::AlignCenter, QString::number( 20, 'f', 0));
    painter->drawText(-135, 0, 20, 20, Qt::AlignCenter, QString::number( 30, 'f', 0));
    painter->drawText(-170, 0, 20, 20, Qt::AlignCenter, QString::number( 40, 'f', 0));
    painter->drawText(-200, 0, 20, 20, Qt::AlignCenter, QString::number( 50, 'f', 0));
    painter->drawText(-227, 0, 20, 20, Qt::AlignCenter, QString::number( 60, 'f', 0));
    painter->drawText(-260, 0, 20, 20, Qt::AlignCenter, QString::number( 90, 'f', 0));

    painter->drawText(-275, -10, 20, 20, Qt::AlignCenter, QString::number(  0, 'f', 0));
    painter->drawText(-270, -53, 20, 20, Qt::AlignCenter, QString::number( 10, 'f', 0));
    painter->drawText(-260, -95, 20, 20, Qt::AlignCenter, QString::number( 20, 'f', 0));
    painter->drawText(-245,-135, 20, 20, Qt::AlignCenter, QString::number( 30, 'f', 0));
    painter->drawText(-220,-170, 20, 20, Qt::AlignCenter, QString::number( 40, 'f', 0));
    painter->drawText(-190,-200, 20, 20, Qt::AlignCenter, QString::number( 50, 'f', 0));
    painter->drawText(-150,-230, 20, 20, Qt::AlignCenter, QString::number( 60, 'f', 0));
    painter->drawText(-100,-255, 20, 20, Qt::AlignCenter, QString::number( 70, 'f', 0));
    painter->drawText( -50,-267, 20, 20, Qt::AlignCenter, QString::number( 80, 'f', 0));
    painter->drawText( -10,-270, 20, 20, Qt::AlignCenter, QString::number( 90, 'f', 0));

    painter->drawText(-260,-270, 90, 20, Qt::AlignLeft, QString("Kite course"));
    painter->drawText(-260,-250, 90, 20, Qt::AlignLeft, QString("Kite heading"));

    QLOG_DEBUG()<<"DisplayBackground paint...";
}
// join into one method
void DisplayBackground::computeLongitude()
{
    QPolygonF polygonLongitude;
    double x, y;
    for (int i = -90 ; i <= 90 ; i = i + 10){
        for (int  latitude = 0 ; latitude <= 90 ; latitude = latitude + 5) {
            x =  250 * cos(latitude * M_PI / 180.0) * sin( i * M_PI / 180.0);
            y =  ( -250 * sin(latitude * M_PI / 180));
            polygonLongitude << QPointF(x,y);
        }
        longitudePolygons.append(polygonLongitude);
        polygonLongitude.clear();
    }
    QLOG_DEBUG()<<"DisplayBackground longitude calculation...";
}

void DisplayBackground::computeLatitude()
{
    QPolygonF polygonLatitude;
    double x, y;
    for (int i = 0 ; i <= 90 ; i = i + 10) {
        for (int longitude = -90; longitude <= 90 ; longitude = longitude + 5) {
            x =  250 * cos(i * M_PI / 180.0) * sin(longitude * M_PI / 180.0);
            y =  ( -250 * sin(i * M_PI/180));
            polygonLatitude << QPointF(x,y);
        }
        latitudePolygons.append(polygonLatitude);
        polygonLatitude.clear();
    }
    QLOG_DEBUG()<<"DisplayBackground latitude calculation...";
}
