/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#include "displaybearing.h"
#include "QsLog.h"

DisplayBearing::DisplayBearing()
{
    QLOG_DEBUG() << "Created DisplayBearing...";
    m_bearing.append(QPointF(0.0,0.0));
}

QRectF DisplayBearing::boundingRect() const
{
    return QRectF(-280, -150, 560, 300);
}

void DisplayBearing::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setClipRect( option->exposedRect );
    (void) widget;
    painter->translate(0, 125);
    QPen magentaPen(Qt::magenta, 2, Qt::SolidLine, Qt::RoundCap);
    painter->setPen(magentaPen);
    QPainterPath bearingPath;
    if (m_bearing.size() >= 2) {
        bearingPath.moveTo(this->m_bearing[0]);
        for(int i = 1; i <this->m_bearing.size(); i++) {
            bearingPath.lineTo(this->m_bearing[i]);
        }
        painter->drawPath(bearingPath);
    }
    QLOG_DEBUG() << "DisplayBearing: painted...";
}

void DisplayBearing::setKiteBearingParameters(QList<QPointF> bearing)
{
    prepareGeometryChange();
    m_bearing = bearing;
    QLOG_DEBUG() << "DisplayBearing: bearing updated...";
}
