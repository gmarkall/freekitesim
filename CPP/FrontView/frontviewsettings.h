/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef FRONTVIEWSETTINGS_H
#define FRONTVIEWSETTINGS_H

#include <QSettings>
#include <QStringList>


// Responsibility: store settings
class FrontViewSettings
{

public:
    // constructor
    FrontViewSettings(QString fileName);
    // destructor
    ~FrontViewSettings();

    // setters
    void setInputSources(QString inputSources);
    void setTrailLength(int trailLength);
    void setHeadingScale(double headingScale);
    void setCourseScale(double courseScale);
    void setPeriod(int period_ms);
    void setCeilingHeight(double ceilingHeight);
    void setTrajectoryPointSize(int trajectoryPointSize);
    void setTrajectoryStepSize(int stepSize);

    // getters
    QString getInputSources();
    int getTrailLength();
    double getHeadingScale();
    double getCourseScale();
    int getPeriod();
    double getCeilingHeight();
    double getTrajectoryPointSize();
    int getTrajectoryStepSize();

    QString getAllKeys(); // needed for unit tests and constructor
    QSettings* getSettings();

    // methods
    void createDefaultSettings();
    // write the port to QSettings data
    void write();
    // read the port from QSettings data
    void read();

private:
    // attributes
    QSettings* settings;
    QString inputSources;
    int     trailLength;
    double  headingScale;
    double  courseScale;
    int period_ms;
    double ceilingHeight;
    int trajectoryPointSize;
    int trajectoryStepSize;

};

#endif // FRONTVIEWSETTINGS_H
