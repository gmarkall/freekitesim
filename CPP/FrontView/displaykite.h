/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef DISPLAYKITE_H
#define DISPLAYKITE_H

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include <QDebug>
#include <cmath>
#include <QList>
#include <QPointF>
#include <QVector>

// Responsibility: Drawing of the kite related items that display system state variables
class DisplayKite : public QGraphicsItem
{
public:
    // constructor
    DisplayKite();

    // setters
    void setKiteStateParameters(QPointF kitePoint, QPointF headingPoint, QPointF coursePoint,
                                 double headingAngle, double courseAngle);
                                // sets the class attributes related to kite position, heading,
                                // and course.
    void setKiteTrailSize(int trailSize);

    // methods
    // Implementation of a pure virtual public functions of a QGraphicsItem.
    // Returns an estimate of the area painted by the item.
    // Necessary because it is called by the scene where the item is placed.
    QRectF boundingRect() const;

    // Implementation of a pure virtual public functions of a QGraphicsItem.
    // Paints subitems into the foreground item.
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    // clears the trail of the kite
    void clear();

private:
    // attributes
    QPointF m_kitePoint;          // Point representing kite.
    QPointF m_headingPoint;       // Point representing one point for the headingline (the other point is kitePoint).
    QPointF m_coursePoint;        // Point representing one point for the courseline (the other point is kitePoint).
    double  m_headingAngle;       // Heading angle to be displayed as text on instrument.
    double  m_courseAngle;        // Course angle to be displayed as text on instrument.
    double  m_trailSize;          // Number of points forming the length of the kite trail.
    QList<QPointF> m_kiteTrail;   // List of kite points forming the kite trail limited by size from settings.
};

#endif // DISPLAYKITE_H
