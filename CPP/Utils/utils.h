/*
* This file is part of FreeKiteSim.
*
* FreeKiteSim -- A kite-power system power simulation software.
* Copyright (C) 2013 by Uwe Fechner, Delft University
* of Technology, The Netherlands. All rights reserved.
*
* FreeKiteSim is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* FreeKiteSim is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/
#ifndef UTILS_H
#define UTILS_H
#include <math.h>
#include <QString>
#include <QDateTime>

// utility functions, needed for the WinchGUI project

class Utils
{
public:
    explicit Utils();
    // return the time since epoch in seconds; microsecond resolution; Linux only
    static double timeInSec();

    static QDateTime timeObject(double time);

    // create a delay with a microsecond resolution
    static void usleep(unsigned long usecs);

    // convert the linear setting of a knob or wheel into a 1-2-5-10-20-50 etc scaling
    // log125(0)=1; log125(1) = 2;   log125(2) =5;   log125(3)  = 10;
    //              log125(-1)= 0.5; log125(-2)=0.2; log125(-3) = 0.1;
    static double exp125(int linearSetting);

    // inverse function for the exp125 function
    static int log125(double value);

    // round a double value to two decimal digits
    static double round(double value);

    // compare two double values and return true, if the absolute value of the quotient is < 0.1%
    static bool isEqual(double value1, double value2);

    // compare two double values and return true, if the absolute value of the quotient is < 1e-digits%
    static bool isAlmostEqual(double value1, double value2, int digits);

    // y offset for restoring window positions, depending on window manager
    int yOffset();

    // x offset for restoring window positions, depending on window manager
    int xOffset();
private:
    QString m_hostname;
};

#endif // UTILS_H
