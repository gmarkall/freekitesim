# -*- coding: utf-8 -*-
""" Calculate the initial position, force and orientation calculations
    for a 4-point kite, connected to a kite control unit (KCU). """
import math
import numpy as np
# from Settings import HEIGHT_K, HEIGHT_B, WIDTH, SEGMENTS
from numpy import cross
from numpy.linalg import norm

def normalize(vec):
    """ Calculate the normalized vector (norm: one). """
    return vec / norm(vec)

class Kite_4_point(object):
    """ This class implement the initial position, force and orientation calculations
        for a 4-point kite, connected to a kite control unit (KCU). """
    def __init__(self, height_k, height_b, width, segments, m_k):
        self.height_k = height_k
        self.height_b = height_b
        self.width = width
        self.segments = segments
        self.m_k = m_k # relative nose distance, default: 0.2
        print "m_k: ", m_k
        self.pos_kite = np.zeros(3) # position of the kite in the east-north-up (ENU) reference frame
        self.pos1     = np.zeros(3) # position of kite particle one
        self.pos2     = np.zeros(3) # position of kite particle two
        self.pos3     = np.zeros(3) # position of kite particle three
        self.pos4     = np.zeros(3) # position of kite particle four
    def setKitePos(self, pos_kite):
        """ set the kite position in ENU coordinates """
        self.pos_kite = pos_kite

    def setPodPos(self, pos_pod):
        self.pos_pod = pos_pod

    def calcInitialKiteRefFrame(self, vec_c, v_app):
        """ Calculate the initial orientation of the kite based on the last tether segment and
        the apparent wind speed.
        vec_c: (pos_n-2) - (pos_n-1) n: number of particles without the three kite particles
                                        that do not belong to the main thether (P1, P2 and P3).
        """
        z = normalize(vec_c)
        y = normalize(cross(v_app, vec_c))
        x = normalize(cross(y, vec_c))
        return (x, y, z)

    def calcPositions(self, x, y, z, beta = math.pi/2.0):
        """
        Calculate the initial position of the kite particles.
        x, y, z:  the unit vectors of the kite reference frame in the ENU reference frame
        beta: inclination angle of the kite; beta = atan2(-pos_kite[2], pos_kite[1]) ???
        """
        h_kx = self.height_k * math.cos(beta); # print 'h_kx: ', h_kx
        h_kz = self.height_k * math.sin(beta); # print 'h_kz: ', h_kz
        h_bx = self.height_b * math.cos(beta)
        h_bz = self.height_b * math.sin(beta)
        self.pos_kite = self.pos_pod - (h_kz + h_bz) * z + (h_kx + h_bx) * x
        self.pos3 = self.pos_kite + h_kz * z + 0.5 * self.width * y + h_kx * x
        self.pos1 = self.pos_kite + h_kz * z + (h_kx + self.width * self.m_k) * x
        self.pos4 = self.pos_kite + h_kz * z - 0.5 * self.width * y + h_kx * x
        self.pos0 = self.pos_kite + (h_kz + h_bz) * z + (h_kx + h_bx) * x

    def calcPositions2(self, x, y, z):
        """
        Calculate the initial position of the kite particles.
        x, y, z:  the unit vectors of the kite reference frame in the ENU reference frame
        """
        self.pos_centre = self.pos_pod - self.height_b * z
        self.pos3 = self.pos_centre + 0.5 * self.width * y
        self.pos1 = self.pos_centre + (self.width * self.m_k) * x
        self.pos4 = self.pos_centre - 0.5 * self.width * y
        self.pos0 = self.pos_centre - self.height_k * z

    def getParticle(self, index):
        if index == self.segments - 1:
            return self.pos0
        elif index == self.segments:
            return self.pos_kite
        elif index == self.segments + 1:
            return self.pos3
        elif index == self.segments + 2:
            return self.pos1
        elif index == self.segments + 3:
            return self.pos4
    def calcInitialSegmentLengths(self, particle1, particle2):
        """ Calculate the inital segment lengths. Valid input range:
            particle1, particle2: SEGMENTS - 1 .. SEGMENTS + 3
            SEGMENTS - 1: index of KCU
            SEGMENTS    : index of kite particle
        """
        delta = self.getParticle(particle2) - self.getParticle(particle1)
        return norm(delta)

    def getParticles(self):
        pod_pos = np.array(( 75., 0., 129.90381057))
        vec_c    = np.array((-15., 0., -25.98076211))
        v_app    = np.array((10.4855, 0, -3.08324))
        self.setPodPos(pod_pos)
        x, y, z = self.calcInitialKiteRefFrame(vec_c, v_app)
        # print 'x, y, z: ', x, y, z
        self.calcPositions(x, y, z)
        a = np.zeros(3)
        b = self.pos0
        c = self.pos1
        d = self.pos_kite
        e = self.pos3
        f = self.pos4
        return np.vstack((a, b, c, d, e, f))

if __name__ == '__main__'    :
    if False:
        pod_pos = np.array(( 75., 0., 129.90381057))
        vec_c    = np.array((-15., 0., -25.98076211))
        v_app    = np.array((10.4855, 0, -3.08324))
        from Settings import HEIGHT_K, HEIGHT_B, WIDTH, SEGMENTS
        kite = Kite_4_point(HEIGHT_K, HEIGHT_B, WIDTH, SEGMENTS)
        kite.setPodPos(pod_pos)
        x, y, z = kite.calcInitialKiteRefFrame(vec_c, v_app)
        print 'x, y, z: ', x, y, z
        kite.calcPositions(x, y, z)
        print 'pos_4     (now: 0)  : ', kite.pos0
        print 'pos_2     (now: 1)  : ', kite.pos1
        print 'pos_kite: (now: 2)  : ', kite.pos_kite
        print 'pos_1     (now: 3)  : ', kite.pos3
        print 'pos_3     (now: 4)  : ', kite.pos4
        print 'last_segment_length: ', kite.calcInitialSegmentLengths(SEGMENTS - 1, SEGMENTS)
    else:
        from Settings import HEIGHT_K, HEIGHT_B, WIDTH, SEGMENTS, M_k
        kite = Kite_4_point(HEIGHT_K, HEIGHT_B, WIDTH, SEGMENTS, M_k)
        print kite.getParticles()

