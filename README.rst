Free Kite Power System Simulator
--------------------------------

This project aims at the development of a free kite-power system simulator
for educational purposes.

Intended use:

a) Provide a learning environment for students who are studying airborne
   wind energy.
b) Training of kite pilots and winch operators. In this case kite and winch
   are operated with joysticks.
c) Development and test of automatic control software for kite-power systems.

This software is published under the LGPL license.

If you have any suggestions and fixes to improve this software please write
a posting on https://groups.google.com/forum/#!forum/free-kitesim .

Installation
------------

The installation was tested on Ubuntu 12.04 only, but it should work on any modern
64 bit Linux computer. 

.. The manual installation is explained in:
.. `INSTALL_01.txt <http://bitbucket.org/ufechner/freekitesim/src/master/INSTALL_01.txt>`_
..
.. For fast, dynamic simulations the installation of numba (a Python compiler) is
.. required, which is explained in:
.. `INSTALL_02.txt <http://bitbucket.org/ufechner/freekitesim/src/master/INSTALL_02.txt>`_
..

Installation now works using an installation script written in python and using
conda packages uploaded to https://conda.binstar.org. 
Four dependencies will NOT be installed with this script, namely gfortran, freeglut3, git and unxz
so these will need to be installed on the system. For ubuntu just run::

    sudo apt-get install gfortran freeglut3 git xz-utils

To install FreeKiteSim now, the repository is cloned first and then the dependencies are installed with
the installation script::

    git clone https://bitbucket.org/ufechner/FreeKiteSim.git ~/00PythonSoftware/FreeKiteSim
    cd ~/00PythonSoftware/FreeKiteSim
    python fks_control install-base

If you also would like to install spyder do the following::

    python fks_control install spyder_full

If you questions regarding the installation or the usage, please write
a posting on https://groups.google.com/forum/#!forum/free-kitesim .

How to use
----------

The Software can be used in two different modes at this point. 

- The log-file viewer allows to replay test-flights by reading the recorded data
  and showing the movement in a 3D animation 
- The dynamic simulator offers the user the possiblity to tweak parameters and
  test control software with a four-point kite model

The two different programs can be run with the executable scripts kitesim.sh and
kiteplayer.sh. Obviously the former runs the simulation and the other the
log-file viewer. 

How to use the log-file viewer
------------------------------
a) Unpack compressed log files with the commands::

    cd ~/00PythonSoftware/FreeKiteSim/00_results
    unxz *.xz

b) Run the 3D viewer and the GUI-based log file player::

    cd ~/00PythonSoftware/FreeKiteSim
    ./kiteplayer.sh

c) Rearange the windows so that they do not overlap. Select a log file
   in the combobox on the left and press play.

Please look also at the `FreeKiteSim <freekitesim/src/master/doc/FreeKiteSim.rst>`_ manual.
A .pdf version of the documentation can be created with the following commands::

    cd doc
    ./make.sh

How to use the dynamic simulator
--------------------------------

The dynamic simulation is done with the RADAU solver from the ASSIMULO solver suite.
One simple example can be found in the example folder.

The following files belong to the dynamic simulator:

- Timer.py        (timer class, used for benchmarking)
- linalg_3d.py    (fast 3D linear algebra routines)
- KCU_Sim.py      (simulator for the kite control unit)
- KPS4P.py        (Python version of the 4-point kite model)
- WinchModel.py   (model of the 20 kW winch of TU Delft)
- Kite_4_point.py (script to construct the initial positions of the kite particles)
- RTSim.py        (simulator core, based on Assimulo and using the RADAU solver)
- KiteViewer3.py  (extended version of the 3D kite viewer)
- Logger.py       (Saves the simulation data to a log file if wanted)

To run the simulation:

1. connect a joystick, e.g. Logitech Gamepad F310 or Logitech Extreme 3D Pro (prefered)
2. open a terminal and enter::

    cd ~/00PythonSoftware/FreeKiteSim/
    ./kitesim.sh

3. wait until the message "Simulation started!" is printed in the console

Now you can steer by pushing the joystick to the right or to the left, and operate the
winch by turning (rotating the stick around it's vertical axis) the joystick (if you use 
the Extreme 3D joystick).

Apart from the steering, also the depower settings can be influenced with the
joystick. Depowering can be used to lower the forces on the kite in order to
reel in with high speeds. Low depower settings will increase the forces and the
power but might lead to structural failures of the kite. The three possible actions are:

- Maximum depower setting - Button 1 + Button 5
- Minimum depower setting - Button 1 + Button 3
- Normal depower setting  - Button 1 + Button 2

The numbering of the buttons might be imprinted on the joystick itself as is the
case for the Extreme 3D joystick. Otherwise it can be tested with the joy_pad.py
script in the main directory which prints the pressed buttons. For the Extreme
3D joystick numbering see the image below.

.. image:: https://bitbucket.org/ufechner/freekitesim/raw/master/doc/joystick_button_numbering.png

The simulation can be stopped by selecting the terminal window in which the
simulation runs and pressing ^C (Control+c). Then the user will be asked whether
the flight data should be saved as a log-file for later reviewing with the
log-file player. Typing 'yes' will require you to specify the name of the
log-file (a .df is automatically added as the extension so only the actual name
is required).

The simulation speed is not yet realtime, but about four times slower than realtime on a core i7, 3.4 GHz.

Delft University of Technology, section wind energy, The Netherlands.