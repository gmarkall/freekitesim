Version 0.3
-----------

- Added logging of simulation data. The created logfiles can then be replayed
  with the kiteplayer.
- Increased simulation speed by using lookup tables instead of interpolation in
  every step. Current execution time of KPS4P.py on a Core i5-4670@3.4GHz is about 70μs, 
  30μs are needed for running the simulation in real-time.

Version 0.2
-----------

- Added installation script for Ubuntu 12.04, 64 bits. It should work on any modern 64 bit
  Linux installation.

Version 0.1
-----------

- Added fully working 4-point kite model with segmented tether, reel-out and depower capable.
- Complete installation instructions for manual installation on Ubuntu 12.04, 64 bits.
